#ifndef CTRHEADER_H_H_H
#define CTRHEADER_H_H_H

// #ifdef CTRCENTER_EXPORTS
// #define CONTROL_DLL __declspec(dllexport)
// #else
// #define CONTROL_DLL __declspec(dllimport)
// #endif

#define CONTROL_DLL

/***函数库引入***/
//引入PMAC库
#include "CtrlPmacruntime.h"

/***自定义常量***/
#define CARD_NUM		  0				//0号PMAC卡

#define WM_PMAC_INTERRUPT WM_USER+2		//中断消息
#define ISR_IR6			  0x40			//6号中断源


#endif