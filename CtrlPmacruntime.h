#include <windows.h>

#ifndef RUNTIME_H_H_H
#define RUNTIME_H_H_H

// ************************************************************************
// COMM_BUFFER status
// ************************************************************************
// Communication return codes that appear in COMM_BUFFER status
#define COMM_EOT               0x80000000
#define COMM_TIMEOUT           0xC0000000
#define COMM_ERROR             0xE0000000
#define COMM_FAIL              0xF0000000
#define COMM_ANYERR            0x70000000

// Masks for determining what return codes are in COMM_BUFFER status
#define COMM_CHARS(c)          (c & 0x0FFFFFFF)
#define COMM_STATUS(c)         (c & 0xF0000000)
#define COMM_OK                1
#define COMM_UNSOLICITED       0x10000000
#define IS_COMM_MORE(c)        ((c & 0xF0000000) == 0)
#define IS_COMM_EOT(c)         ((c & 0xF0000000) == COMM_EOT)
#define IS_COMM_TIMEOUT(c)     ((c & 0xF0000000) == COMM_TIMEOUT)
#define IS_COMM_ERROR(c)       ((c & 0xF0000000) == COMM_ERROR)
#define IS_COMM_FAIL(c)        ((c & 0xF0000000) == COMM_FAIL)
#define IS_COMM_ANYERROR(c)    ((c & 0x70000000) == COMM_ANYERR)
#define IS_COMM_UNSOLICITED(c) ((c & 0xF0000000) == COMM_UNSOLICITED)

// ************************************************************************
// COMM Type Defines
// ************************************************************************
// Maximum length of a response
#define MAX_COMM_BUFFER        256
#define MAX_DPRBUFFER          160
#define MAX_DPRREADBUFFER      256

typedef BOOL		(_stdcall * OPENPMACDEVICE)(DWORD dwDevice);
typedef BOOL		(_stdcall * CLOSEPMACDEVICE)(DWORD dwDevice);
typedef long		(_stdcall * PMACSELECT)( HWND hwnd );

typedef int			(_stdcall * GETRESPONSEA)(DWORD dwDevice,LPTSTR s,UINT maxchar,LPCTSTR outstr);
typedef int			(_stdcall * GETRESPONSEEXA)(DWORD dwDevice,LPTSTR s,UINT maxchar,LPCTSTR outstr);
typedef short int   (_stdcall * PMACGETVARIABLE)(DWORD dwDevice,char ch,UINT num,short int def);
typedef long		(_stdcall * PMACGETVARIABLELONG)(DWORD dwDevice,char ch,UINT num,long def);
typedef double		(_stdcall * PMACGETVARIABLEDOUBLE)(DWORD dwDevice,char ch,UINT num,double def);
typedef void		(_stdcall * PMACSETVARIABLE) (DWORD dwDevice, char ch, UINT num, short int val);
typedef void		(_stdcall * PMACSETVARIABLELONG) (DWORD dwDevice, char ch, UINT num, long val);
typedef void		(_stdcall * PMACSETVARIABLEDOUBLE) (DWORD dwDevice, char ch, UINT num, double val);

typedef struct _INTRBUFFER 
{ 
  PCH     lpData;				// pointer to reserved buffer data area
  ULONG   dwBufferLength;		// pointer to reserved length of buffer 
  DWORD   dwInterruptType;		// Identifies PMAC Interrupt number that triggered callback function.       
} INTRBUFFER, * PINTRBUFFER;

typedef void	(_stdcall * PMACINTRPROC) ( DWORD msg, PINTRBUFFER pBuffer );
typedef BOOL	(_stdcall * PMACINTRFUNCINIT)(DWORD dwDevice,PMACINTRPROC pFunc,DWORD msg,ULONG ulMask);
typedef BOOL	(_stdcall * PMACINTRTERMINAGE)( DWORD dwDevice );

typedef long	(_stdcall * DOWNLOAD)(DWORD dwDevice,DWORD/*DOWNLOADMSGPROC*/ msgp,/*DOWNLOADGETPROC*/DWORD getp,
                            DWORD/*DOWNLOADPROGRESS*/ ppgr,PCHAR filename,BOOL macro,BOOL map,BOOL log,BOOL dnld);

// ************************************************************************
// COMM Functions
// ************************************************************************
HINSTANCE	PmacRuntimeLink(const DWORD dwDeviceNum);
void		CloseRuntimeLink(const DWORD dwDeviceNum);

#ifdef __cplusplus
extern "C" {
#endif

extern OPENPMACDEVICE			OpenPmacDevice;
extern CLOSEPMACDEVICE			ClosePmacDevice;
extern PMACSELECT				PmacSelect;
extern GETRESPONSEA				PmacGetResponse;
extern GETRESPONSEEXA			PmacGetResponseEx;
extern PMACGETVARIABLE			PmacGetVariable;
extern PMACGETVARIABLELONG		PmacGetVariableLong;
extern PMACGETVARIABLEDOUBLE	PmacGetVariableDouble;
extern PMACSETVARIABLE			PmacSetVariable;
extern PMACSETVARIABLELONG		PmacSetVariableLong;
extern PMACSETVARIABLEDOUBLE	PmacSetVariableDouble;
extern PMACINTRFUNCINIT			PmacINTRFuncCallInit;  
extern PMACINTRTERMINAGE		PmacINTRTerminate;
extern DOWNLOAD					PmacDownload;

#ifdef __cplusplus
};
#endif

#endif
