#ifndef CTRCENTER_H_H_H
#define CTRCENTER_H_H_H

#include "CtrHeader.h"
#include "CtrPmac.h"
#include "CtrIO.h"
#include "CtrMotor.h"
#include "CtrParam.h"
#include "CtrProg.h"

class CONTROL_DLL CCtrCenter
{
public:
	CCtrCenter();
	virtual ~CCtrCenter();
	BOOL InitialControl(PMACINTRPROC pmacIntProc=(CCtrCenter::InterruptFunc));

	static void WINAPI InterruptFunc(DWORD msg, PINTRBUFFER pBuffer);

private:
	HINSTANCE	m_hPmacLink;

public:
	CCtrPmac	*m_pPmac;
	CCtrMotor	*m_pMotorDownX;
	CCtrMotor	*m_pMotorDownY;
	CCtrMotor	*m_pMotorUpX;
	CCtrMotor	*m_pMotorUpY;
	CCtrMotor	*m_pMotorUpZ;
	CCtrProg	*m_pProgDownXHome;
	CCtrProg	*m_pProgDownYHome;
	CCtrParam	*m_pParam;
	CCtrIO		*m_pIO;
	BOOL		m_bIsInit;
};

#endif