#ifndef CTRPROG_H_H_H
#define CTRPROG_H_H_H

class CONTROL_DLL CCtrProg
{
public:
	CCtrProg(const UINT uAxisIndex, const UINT uProgIndex, CCtrPmac *pCtrPmac);
	CCtrProg(const UINT uPLCIndex, CCtrPmac *pCtrPmac);
	virtual ~CCtrProg();

	void Run();
	void Stop();
	void Enable();
	void Disable();
	BOOL Download(PCHAR szFileName );

private:
	UINT m_uAxisIndex;
	UINT m_uProgIndex;
	UINT m_uPLCIndex;
	CCtrPmac *m_pCtrPmac;
	char m_szCommand[255];
};

#endif