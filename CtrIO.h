#ifndef CTRIO_H_H_H
#define CTRIO_H_H_H

class CONTROL_DLL CCtrIO
{
public:
	CCtrIO(CCtrPmac *pCtrPmac);
	virtual ~CCtrIO();
	void On(const UINT uIndex);
	void Off(const UINT uIndex);
	BOOL GetState(const UINT uIndex);
private:
	CCtrPmac *m_pCtrPmac;
};

#endif