#ifndef CTRPMAC_H_H_H
#define CTRPMAC_H_H_H

class CONTROL_DLL CCtrPmac
{

public:
	CCtrPmac(const UINT uCardNum=0);
	virtual ~CCtrPmac();
	int GiveCommand(LPCTSTR szCommand);
	int GetResponse(LPTSTR szResponse, LPCTSTR szCommand);
	short int GetShort(const char cType, const UINT nIndex, const short int snDef=0);
	long GetLong(const char cType, const UINT nIndex, const long lnDef=0);
	double GetDouble(const char cType, const UINT nIndex, const double dbDef=0);
	void SetShort(const char cType, const UINT nIndex, const short int snValSet);
	void SetLong(const char cType, const UINT nIndex, const long lnValSet);
	void SetDouble(const char cType, const UINT nIndex, const double dbValSet);
	BOOL DownloadFile(PCHAR szFileName);

private:
	UINT m_uCardNum;

	UINT m_uMaxChar;		//复制的最大字符串数
	CHAR m_szResponse[255];	//接受字符数组
};

#endif