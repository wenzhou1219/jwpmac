/*************************************************************************
  File: runtime.cpp - Dynamic link functions.
**************************************************************************/

#include <StdAfx.h>
#include "CtrlPmacruntime.h"

//************************************************************************
// Function instants for runtime linking
//************************************************************************

OPENPMACDEVICE			OpenPmacDevice;
CLOSEPMACDEVICE			ClosePmacDevice;
PMACSELECT				PmacSelect;
GETRESPONSEA			PmacGetResponse;
GETRESPONSEEXA			PmacGetResponseEx;
PMACGETVARIABLE			PmacGetVariable;
PMACGETVARIABLELONG		PmacGetVariableLong;
PMACGETVARIABLEDOUBLE	PmacGetVariableDouble;
PMACSETVARIABLE			PmacSetVariable;
PMACSETVARIABLELONG		PmacSetVariableLong;
PMACSETVARIABLEDOUBLE	PmacSetVariableDouble;
PMACINTRTERMINAGE		PmacINTRTerminate;
PMACINTRFUNCINIT		PmacINTRFuncCallInit;
DOWNLOAD				PmacDownload;
    
//这两个全局变量不让其他文件引用
static HINSTANCE	hLib		= NULL ;
static BOOL			bDriverOpen = FALSE ;

HINSTANCE PmacRuntimeLink(const DWORD dwDeviceNum)
{
  int i;

  // Get handle to PComm32.DLL
  hLib = LoadLibrary("PComm32");

  if( hLib != NULL )
  {
    for (i = 0 ; i < 1 ; i++)
    {
		OpenPmacDevice = (OPENPMACDEVICE)GetProcAddress(hLib,"OpenPmacDevice");
		if(OpenPmacDevice==NULL) break;
		ClosePmacDevice = (CLOSEPMACDEVICE)GetProcAddress(hLib,"ClosePmacDevice");
		if(ClosePmacDevice==NULL) break;
		PmacSelect = (PMACSELECT)GetProcAddress(hLib,"PmacSelect");
		if(PmacSelect==NULL) break;
		PmacGetResponse = (GETRESPONSEA)GetProcAddress(hLib,"PmacGetResponseA");
		if(PmacGetResponse==NULL) break;
		PmacGetResponseEx = (GETRESPONSEEXA)GetProcAddress(hLib,"PmacGetResponseExA");
		if(PmacGetResponseEx==NULL) break;
		PmacGetVariable = (PMACGETVARIABLE)GetProcAddress(hLib,"PmacGetVariable");
		if(PmacGetVariable==NULL) break;
		PmacGetVariableLong = (PMACGETVARIABLELONG)GetProcAddress(hLib,"PmacGetVariableLong");
		if(PmacGetVariableLong==NULL) break;
		PmacGetVariableDouble = (PMACGETVARIABLEDOUBLE)GetProcAddress(hLib,"PmacGetVariableDouble");
		if(PmacGetVariableDouble==NULL) break;
		PmacSetVariable = (PMACSETVARIABLE)GetProcAddress(hLib,"PmacSetVariable");
		if(PmacSetVariable==NULL) break;
		PmacSetVariableLong = (PMACSETVARIABLELONG)GetProcAddress(hLib,"PmacSetVariableLong");
		if(PmacSetVariableLong==NULL) break;
		PmacSetVariableDouble = (PMACSETVARIABLEDOUBLE)GetProcAddress(hLib,"PmacSetVariableDouble");
		if(PmacSetVariableDouble==NULL) break;
		PmacINTRTerminate = (PMACINTRTERMINAGE)GetProcAddress(hLib,"PmacINTRTerminate");
		if(PmacINTRTerminate==NULL) break;
		PmacINTRFuncCallInit = (PMACINTRFUNCINIT)GetProcAddress(hLib,"PmacINTRFuncCallInit");
		if(PmacINTRFuncCallInit==NULL) break;
		PmacDownload = (DOWNLOAD)GetProcAddress(hLib,"PmacDownloadA");
		if(PmacDownload==NULL) break;
  	}

    if(i==0)
    {
      FreeLibrary(hLib);
      hLib = NULL;
    }
	else
	{
		//加载动态链接库和打开PMAC卡成功则不再打开，保证只打开一次
		if(bDriverOpen)
		{
			return hLib;
		}

		bDriverOpen = OpenPmacDevice(dwDeviceNum);

		if (FALSE == bDriverOpen)
		{
			return NULL ;
		}
		else
		{
			return hLib;
		}
	}
  }

  return hLib;
}

void CloseRuntimeLink(const DWORD dwDeviceNum)
{
	if(bDriverOpen) 
	{
		bDriverOpen = !ClosePmacDevice(dwDeviceNum);
	}

	if(hLib)
	{
		FreeLibrary(hLib);
		hLib = NULL ;
	}
}         
